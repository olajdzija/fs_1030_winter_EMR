import React from 'react'
import './App.css'
import Navigation from './components/shared/Navigation'
import Footer from './components/shared/footer'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import Denied from './components/pages/denied'
import Contact from './components/pages/Contact'
import patient_form from './components/pages/patient_form'
import doctor_report from './components/pages/doctor_report'
import Login from './components/pages/Login'
import Listing from './components/pages/Listing'
import PrivateRoute from './components/shared/PrivateRoute'

function App() {
  return (
   <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/denied" component={Denied} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/patient_form" component={patient_form} />
          <Route exact path="/doctor_report" component={doctor_report} />
          <Route exact path="/login" component={Login} />
          <PrivateRoute path="/submissions">
            <Listing />
          </PrivateRoute>
        </Switch>
        <Footer />  
    </BrowserRouter>
  )
}

export default App;
