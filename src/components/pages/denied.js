import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap'

const Denied = () => {
    return (
        <Container>
        <Row className="my-5">
            <Col lg="7">
                <img className="img-fluid rounded mb-4 mb-lg-0" src="http://placehold.it/900x400" alt="" />
            </Col>
            <Col lg="5">
                <h1 className="font-weight-light">Access denied</h1>
                <p>Please login before trying to access this page </p>
            </Col>
        </Row>
    </Container>
    )
}

export default Denied