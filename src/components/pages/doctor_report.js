import React, { useState, Component } from 'react'
import "./styles.css";
import { Form, FormGroup, Col, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap'
import DatePicker from "react-datepicker";
import Axios from 'axios';


import "react-datepicker/dist/react-datepicker.css";

const Contact = () => {

   

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [patientFirstName, setPatientFirstName] = useState("")
    const [patientLastName, setPatientLastName] = useState("")
    const [birthDate, setBirthDate] = useState(new Date())
    const [cardNum, setCardNum] = useState("")
    const [consultant, setConsultant] = useState("")
    const [hospital, setHospital] = useState("")
    const [ward, setWard] = useState("")
    const [title, setTitle] = useState("")
    const [history, setHistory] = useState("")
    const [admissionTime, setAdmissionTime] = useState(new Date());
    const [reason, setReason] = useState("")
    const [diagnosis, setDiagnosis] = useState("")
    const [treatment, setTreatment] = useState("")
    const [sideNote, setSideNote] = useState("")

    const submitForm=()=>{
        Axios.post('http://localhost:3000/doctor_report/create',{
            firstName:firstName,
            lastName:lastName,
            birthDate:birthDate,
            consulter:consultant,
            hospital:hospital,
            ward:ward,
            title:title,
            history:history,
            admissionTime:admissionTime,
            reason:reason,
            diagnosis:diagnosis,
            treatment:treatment,
            sideNote:sideNote,
            healthCard:cardNum,
        }).then(()=>{
            console.log('success')
        });
    };



    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/contact_form/entries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({firstName,lastName,patientFirstName,birthDate,cardNum,consultant,hospital,ward,history,admissionTime,reason,diagnosis,treatment,sideNote})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
        } else {
            alert(`Congrats! Submission submitted with id: ${payload.id}`)
        }
    }

    return (
        <Container>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText className="text-white m-0">Please complete during or after discussion with patient</CardText>
                </CardBody>
            </Card>
            <Form className="my-5" onSubmit={formSubmit}>
            <FormGroup row>
                    <Label for="firstNameEntry" sm={2}>medical worker first name Name</Label>
                    <Col sm={10}>
                    <Input type="firstName" name="firstName" id="firstNameEntry" placeholder="Enter your first name" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="lastNameEntry" sm={2}>medical worker last name Name</Label>
                    <Col sm={10}>
                    <Input type="lastName" name="lastName" id="lastNameEntry" placeholder="Enter your last name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="titleEntry" sm={2}>Prefix</Label>
                    <Col sm={10}>
                    <select type="title" name="title" id="titleEntry" placeholder="Enter proffession"  required value={title} onChange={e => setTitle(e.target.value) }>
                            <option value="Dr">Doctor</option>
                            <option value="N">Nurse</option>
                            <option value="Pha">Pharmacist</option>
                            <option value="Phy">Physician</option>
                            <option value="T">Therapist</option>
                            <option value="S">Surgeon</option>
                        </select>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="consultantEntry" sm={2}>referrer</Label>
                    <Col sm={10}>
                    <Input type="consultantName" name="consultantName" id="consultantEntry" placeholder="gaurdian, secondary care provider...etc" required value={consultant} onChange={e => setConsultant(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="patientFirstNameEntry" sm={2}>patient first name Name</Label>
                    <Col sm={10}>
                    <Input type="patientFirstName" name="patientFirstName" id="patientFirstNameEntry" placeholder="Enter patient first name" required value={patientFirstName} onChange={e => setPatientFirstName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="patientLastNameEntry" sm={2}>patient last name Name</Label>
                    <Col sm={10}>
                    <Input type="patientLastName" name="patientLastName" id="patientLastNameEntry" placeholder="Enter patient last name" required value={patientLastName} onChange={e => setPatientLastName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="birthDateEntry" sm={2}>Date of birth</Label>
                    <Col sm={10}>
                    <DatePicker selected={birthDate} onChange={(date) => setBirthDate(date)} />
                    </Col>
                </FormGroup>
                
            <FormGroup row>
                    <Label for="cardEntry" sm={2}>health card number</Label>
                    <Col sm={10}>
                    <Input type="card" name="card" id="cardEntry" placeholder="Enter patient health card number"  required value={cardNum} onChange={e => setCardNum(e.target.value) }/>
                    </Col>
                </FormGroup>
                
                <FormGroup row>
                    <Label for="hospitalEntry" sm={2}>Hospital</Label>
                    <Col sm={10}>
                    <Input type="hospital" name="hospital" id="hospitalEntry" placeholder="Enter name of hospital"  required value={hospital} onChange={e => setHospital(e.target.value) }/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="wardEntry" sm={2}>Hospital ward</Label>
                    <Col sm={10}>
                    <Input type="ward" name="ward" id="wardEntry" placeholder="Enter name of hospital ward"  required value={ward} onChange={e => setWard(e.target.value) }/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="addmissionTimeEntry" sm={2}>Time of addmission</Label>
                    <Col sm={10}>
                    <DatePicker selected={admissionTime} onChange={(date) => setAdmissionTime(date)} />
                    </Col>
                </FormGroup>
               

                <FormGroup row>
                    <Label for="historyEntry" sm={2}>patient medical history</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="history" id="historyEntry" required value={history} onChange={e => setHistory(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="reasonEntry" sm={2}>reason for patient visit</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="reason" id="reasonEntry" required value={reason} onChange={e => setReason(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="diagnosisEntry" sm={2}>evaluation of patient's issue</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="diagnosis" id="diagnosisEntry" required value={diagnosis} onChange={e => setDiagnosis(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="treatmentEntry" sm={2}>treatment plan</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="treatment" id="reasonEntry" required value={treatment} onChange={e => setTreatment(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="sideNoteEntry" sm={2}>reason for patient visit</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="sideNote" id="sideNote" required value={sideNote} onChange={e => setSideNote(e.target.value)}/>
                    </Col>
                </FormGroup>



                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                    <Button color="success" onClick={submitForm}>Submit</Button>
                    </Col>
                </FormGroup>
                
            </Form>
        </Container>
      )
    }

    export default Contact