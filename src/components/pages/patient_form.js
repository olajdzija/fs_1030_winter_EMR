import React, { useState } from 'react'
import { Form, FormGroup, Col, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap'
import DatePicker from "react-datepicker";
import Axios from 'axios';

import "react-datepicker/dist/react-datepicker.css";


const Contact = () => {
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [cardNum, setCardNum] = useState("")
    const [address, setAddress] = useState("")
    const [vaccineStatus, setVaccineStatus] = useState("")
    const [appointmentDate, setAppointmentDate] = useState(new Date())
    const [birthDate, setBirthDate] = useState(new Date())
    const [healthConditions, setHealthConditions] = useState("")
    const [race, setRace] = useState("")
    const [gender, setGender] = useState("")
    const [sex, setSex] = useState("")
    const [medication, setMedication] = useState("")
    const [allergies, setAllergies] = useState("")
    const [email, setEmail] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [content, setContent] = useState("")

    const submitForm=()=>{
        Axios.post('http://localhost:3000/patient_form/create',{
            firstName:firstName,
            lastName:lastName,
            birthDate:birthDate,
            ethnicity:race,
            gender:gender,
            healthCard:cardNum,
            email:email,
            phoneNumber:phoneNumber,
            address:address,
            vaccineStatus:vaccineStatus,
            allergies:allergies,
            healthCondition:healthConditions,
            medication:medication,
            appointmentDate:appointmentDate,
            content:content
        }).then(()=>{
            console.log('success')
        });
    };

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/contact_form/entries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({firstName,lastName,address,medication,allergies, email,vaccineStatus,birthDate,race,gender, phoneNumber, content, healthConditions,sex})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
        } else {
            alert(`Congrats! Submission submitted with id: ${payload.id}`)
        }
    }

    return (
        <Container>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText className="text-white m-0">Please complete prior to your visit</CardText>
                </CardBody>
            </Card>
            <Form className="my-5" onSubmit={formSubmit}>
            <FormGroup row>
                    <Label for="firstNameEntry" sm={2}>first name Name</Label>
                    <Col sm={10}>
                    <Input type="firstName" name="firstName" id="firstNameEntry" placeholder="Enter your first name" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="lastNameEntry" sm={2}>last name Name</Label>
                    <Col sm={10}>
                    <Input type="lastName" name="lastName" id="lastNameEntry" placeholder="Enter your last name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="birthDateEntry" sm={2}>Date of birth</Label>
                    <Col sm={10}>
                    <DatePicker selected={birthDate} onChange={(date) => setBirthDate(date)} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="raceEntry" sm={2}>Ethnic group</Label>
                    <Col sm={10}>
                    <select type="race" name="race or ethnicity" id="raceEntry" placeholder="Enter race"  required value={sex} onChange={e => setSex(e.target.value) }>
                            <option value="none">No group Selected</option>
                            <option value="black">Black: African, Afro-Caribbean, African-Canadian descent</option>
                            <option value="East Asian">East Asian: Chinese, Korean, Japanese, Taiwanese descent</option>
                            <option value="Southeast Asian">Southeast Asian: Filipino, Vietnamese, Cambodian, Thai, Indonesian, other Southeast Asian descent</option>
                            <option value="Indigenous">Indigenous: First Nations, Inuit or Métis</option>
                            <option value="Latin American">Latin American: Hispanic descent</option>
                            <option value="Middle Eastern">Middle Eastern: Arab, Persian, West Asian descent, such as Afghan, Egyptian, Iranian, Lebanese, Turkish, Kurdish, etc.</option>
                            <option value="South Asian">South Asian: South Asian descent, e.g. East Indian, Pakistani, Bangladeshi, Sri Lankan, IndoCaribbean, etc.</option>
                            <option value="White">White: European descent</option>
                            <option value="other">Another race category not listed</option>
                        </select>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="genderEntry" sm={2}>Gender</Label>
                    <Col sm={10}>
                    <select type="gender" name="gender" id="genderEntry" placeholder="Enter gender identity"  required value={gender} onChange={e => setGender(e.target.value) }>
                    <option value="none">No group Selected</option>
                            <option value="male">male</option>
                            <option value="female">female</option>
                            <option value="intersex">intersex</option>
                            <option value="non-binary">non-binary</option>
                            <option value="two_spirit">two-spirit</option>
                            <option value="other">not listed</option>
                        </select>
                    </Col>
                </FormGroup>
               
            <FormGroup row>
                    <Label for="cardEntry" sm={2}>health card number</Label>
                    <Col sm={10}>
                    <Input type="card" name="card" id="cardEntry" placeholder="Enter health card number"  required value={cardNum} onChange={e => setCardNum(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="emailEntry" sm={2}>Email</Label>
                    <Col sm={10}>
                    <Input type="email" name="email" id="emailEntry" placeholder="Enter email to contact"  required value={email} onChange={e => setEmail(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="phoneEntry" sm={2}>Phone Number</Label>
                    <Col sm={10}>
                    <Input type="phone" name="phone" id="phoneEntry" placeholder="Enter phone number" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="addressEntry" sm={2}>Street address</Label>
                    <Col sm={10}>
                    <Input type="address" name="address" id="addressEntry" placeholder="Enter address" value={address} onChange={e => setAddress(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="vaccineEntry" sm={2}>vaccine status</Label>
                    <Col sm={10}>
                    <Input type="vaccine" name="vaccine" id="vaccineEntry" placeholder="Enter vaccine status"  required value={vaccineStatus} onChange={e => setVaccineStatus(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="allergyEntry" sm={2}>Allergies</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="text"  id="alergyEntry" placeholder="Enter any allergies"  required value={allergies} onChange={e => setAllergies(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="healthConditionsEntry" sm={2}>Health conditions</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="text"  id="healthConditionsEntry" placeholder="Enter any health conditions you have"  required value={healthConditions} onChange={e => setHealthConditions(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="medicationEntry" sm={2}>Medications</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="text"  id="medicationEntry" placeholder="Enter any medications you are taking"  required value={medication} onChange={e => setMedication(e.target.value) }/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="appointmentDateEntry" sm={2}>preffered date of appointment</Label>
                    <Col sm={10}>
                    <DatePicker selected={appointmentDate} onChange={(date) => setAppointmentDate(date)} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="messageEntry" sm={2}>additional notes</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="text" id="messageEntry" required value={content} onChange={e => setContent(e.target.value)}/>
                    </Col>
                </FormGroup>

                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                    <Button color="success" onClick={submitForm}>Submit</Button>
                    </Col>
                </FormGroup>
                
            </Form>

            
        </Container>
      )
    }

    export default Contact